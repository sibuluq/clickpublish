# clickPublish: Record your published catalog article
# Copyright (C) 2022, 2023 CV Java Multi Mandiri
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# ##########
{
    'application' : True,
    'author'      : 'CV Java Multi Mandiri',
    'category'    : 'Sales',
    'data'        : [
        "data/article_request_data.xml",
        "security/clickpublish_groups.xml",
        "security/ir.model.access.csv",
        "security/article_request_security.xml",
        "views/clickpublish_views.xml",
        "views/platform_views.xml",
        "views/website_views.xml",
        "views/article_views.xml",
        "views/product_template_views.xml",
        "views/article_request_views.xml",
    ],
    'depends'     : [
        'base',
        'hr',
        'mail',
        'product',
    ],
    'installable' : True,
    'license'     : "AGPL-3",
    'maintainer'  : "Sunu Haeriadi",
    'name'        : "clickPublish",
    'summary'     : "Record your published catalog article.",
    'version'     : '15.0.1.1.0',
}
