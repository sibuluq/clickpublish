# clickPublish: Record your published catalog article

**clickPublish** merupakan module clickERP untuk mengelola
publikasi artikel produk yang terdaftar pada sistem clickERP pada
situs web pihak ketiga.

## Pemasangan

Modul ini membutuhkan modul-modul berikut agar dapat berfungsi dengan benar.

- base
- hr
- mail
- product

## Konfigurasi dan Penggunaan

### Grup user khusus clickPublish
clickPublish membuat dua grup baru yaitu

1. **Officer**, yang memiliki kewenangan untuk mengelola daftar
publikasi artikel dan permintaan publikasi.
2. **Manager**, yang memiliki semua kewenangan Officer
ditambah dengan kewenangan konfigurasi modul.

Jadi, jangan lupa untuk memasukan user terkait pada grupnya.

### Tidak bisa kirim log note
Partner yang berelasi dengan user harus sudah memiliki
alamat email agar user bisa membuat log note.

## Roadmap

- [x] Menampung informasi publikasi artikel suatu produk.
  - [x] Masing-masing halaman informasi produk menampilkan
daftar artikel-artikel katalog yang berkaitan dengan
produk tersebut.
  - [x] Terdapat satu halaman yang menampilkan keseluruhan
  daftar tautan artikel produk untuk mempermudah pelacakan
  dan pemantauan.
  - [x] Pada catatan publikasi, terdapat informasi penulis
dari artikel tersebut.
- [x] User dapat membuat permintaan publikasi artikel produk.
  - [x] Memfasilitasi berbagai jenis artikel yang diminta.
  - [x] Permintaan artikel bisa sekaligus ke beberapa website.
  - [x] Ketika permintaan dikirimkan, langsung ada tugas
  kepada tim untuk segera mengerjakan.
  - [x] User mengetahui siapa yang menangani permintaannya.
  - [x] Informasi tentang rentang waktu pengerjaan,
  kapan mulai dan kapan harus selesai.
  - [ ] Satu permintaan dapat berisi lebih dari satu produk.
- [ ] Data statistik dan monitoring publikasi produk.
- [x] Data statistik dan monitoring permintaan publikasi produk.

## Credits

The development of this module is supported by [CV Java Multi Mandiri](https://jvm.co.id)
and carried out by [clickoding](https://clickoding.id) as a business unit engaged
in the software development industry.

### Contributors

- Arsya Fathiha Rahman, SMK Telkom Purwokerto
- Opi Tri Fatmawati, SMK Negeri 1 Purwokerto
- Sri Hayyu Ulfa Naufal, CV Java Multi Mandiri
- Sunu Haeriadi, CV Java Multi Mandiri

### Maintainer

This module is maintained by clickoding.

clickoding is a software house that develops business solution applications for
Indonesian business people.

clickoding is proud to be part of JVM.
