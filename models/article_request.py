# clickPublish: Record your published catalog article
# Copyright (C) 2022, 2023 CV Java Multi Mandiri
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# ##########

import datetime
from odoo import _, api, fields, models

class ArticleRequest(models.Model):
    # ################## #
    # Private attributes #
    # ################## #
    _name        = 'clickpublish.article.request'
    _description = "Article publication requests"
    _inherit     = ['mail.activity.mixin', 'mail.thread']

    # ################### #
    # Fields declarations #
    # ################### #
    
    completed_date = fields.Date(string="Fulfilled date")
    deadline       = fields.Date(
        required=True,
        string="Deadline")
    name           = fields.Char(
        default=lambda self:_('Draft'),
        required=True,
        string="Request number")
    note           = fields.Text(string="Additional note")
    state          = fields.Selection(
        default='draft',
        selection=[
            ('draft', 'Draft'),
            ('queue', 'In queue'),
            ('processed', 'Processed'),
            ('done', 'Done'),
            ('cancel', 'Cancel'),
        ],
        string="Status",
    )

    ## Relationship fields
    article_ids      = fields.One2many('clickpublish.article', 'request_id')
    processed_by_uid = fields.Many2one('res.users', string="Processed by")
    product_id       = fields.Many2one('product.template', string="Product", required=True)
    type_ids         = fields.Many2many('clickpublish.article.type', string="Article type", required=True)
    website_ids      = fields.Many2many('clickpublish.website', string="Website", required=True)
    
    # ############ #
    # CRUD methods #
    # ############ #
    @api.model
    def create(self, document):
        if document.get('state', 'draft'):
            document['name'] = _('Draft')
        
        result = super(ArticleRequest, self).create(document)

        return result

    # ############## #
    # Action methods #
    # ############## #
    def action_done(self):
        self.write({
            'state': 'done',
            'completed_date': datetime.datetime.now()
        })

        self.send_message('Hi %s, your article publication request number %s has been completed.' % (self.create_uid.name, self.name))

        return True

    def action_process_request(self):
        self.write({
            'processed_by_uid': self.env.uid,
            'state': 'processed'
        })

        self.set_activity_done("Article publication request number %s has been processed by %s." % (self.name, self.env.user.name))
        self.send_message('Article publication request number %s is processed.' % (self.name))

        return True

    def action_send_request(self):
        for document in self:
            self.write({
                'state': 'queue'
            })

            document.set_additional_subscriber('clickpublish.group_officer')
            document.update_sequence_number()
            document.send_task('Article publication request', 'The request for publication of article number %s has been submitted. Please process it immediately.' % (document.name), 'clickpublish.group_officer')
            document.send_message('Article publication request number %s is submitted.' % (document.name))

        return True

    # ################ #
    # Business methods #
    # ################ #
    def send_message(self, message):
        for document in self:
            document.message_post(body=message)

    def send_task(self, summary, message, user_group):
        group = self.env.ref(user_group)

        for document in self:
            for user in group.users:
                document.activity_schedule(
                    summary=summary,
                    note=message,
                    user_id=user.id
                )

    def set_activity_done(self, message):
        self.activity_ids._action_done(feedback=message)

    def set_additional_subscriber(self, user_group):
        group = self.env.ref(user_group)

        for document in self:
            partner_ids = list(map(lambda x: x.partner_id.id, group.users))

            document.message_subscribe(partner_ids = partner_ids)

    def update_sequence_number(self):
        for document in self:
            if document.state != 'draft' and document.name == _('Draft'):
                document.write({
                    'name': self.env['ir.sequence'].next_by_code('clickpublish.article.request.sequence')
                })
