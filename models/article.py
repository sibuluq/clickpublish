# clickPublish: Record your published catalog article
# Copyright (C) 2022, 2023 CV Java Multi Mandiri
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# ##########

import random
from odoo import api, fields, models

class Article(models.Model):
    _name        = 'clickpublish.article'
    _description = "Publication"

    name           = fields.Char("URL", required=True)
    product_id     = fields.Many2one("product.template", string="Product")
    employee_id    = fields.Many2one("hr.employee", default=lambda self: self._default_employee_id(), string="Author", required=True)
    website_id     = fields.Many2one("clickpublish.website", string="Publication Media", required=True)
    request_id     = fields.Many2one('clickpublish.article.request')
    type_id        = fields.Many2one('clickpublish.article.type', string="Content type")
    published_date = fields.Date(default=lambda self: fields.Date.today(), string="Published Date")

    def _default_employee_id(self):
        employee_record = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1).ids

        return employee_record

class ArticleType(models.Model):
    _name        = 'clickpublish.article.type'
    _description = "Content types"

    name  = fields.Char("Name", required=True)
    color = fields.Integer(default=lambda self: self._get_default_color())

    def _get_default_color(self):
        return random.randint(1, 11)
