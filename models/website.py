# clickPublish: Record your published catalog article
# Copyright (C) 2022, 2023 CV Java Multi Mandiri
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# ##########

from odoo import api, fields, models

class Website(models.Model):
    _name        = 'clickpublish.website'
    _description = "Article publication websites"

    name = fields.Char("Name", required=True)
    display_name = fields.Char(compute='_compute_display_name')
    platform_id = fields.Many2one(comodel_name='publication.platform', required=True)

    @api.depends('name', 'platform_id')
    def _compute_display_name(self):
        for record in self:
            record.display_name = "%s (%s)" % (record.name, record.platform_id.name)

    def name_get(self):
        result = []

        for record in self:
            display_name = record.display_name
            result.append((record.id, display_name))

        return result
