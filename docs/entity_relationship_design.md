# clickPublish Entity Relationship Design

```plantuml
@startuml clickpublish-erd
hide circle
hide empty members
skinparam linetype ortho

' Entities
entity clickpublish.article {
    * name : char
    --
    * product_id : many2one "product.template"
    * employee_id : many2one "hr.employee"
    * website_id : many2one "clickpublish.website"
    request_id : many2one "clickpublish.article.request"
    type_id : many2one "clickpublish.article.type"
}

entity clickpublish.article.type {
    * name : char
    color : integer <<generated>>
}

entity clickpublish.article.request {
    * deadline : date
    * name : char
    note : text
    state : selection default "draft"
    --
    article_ids : one2many "clickpublish.article"
    processed_by_uid : many2one "res.users"
    * product_id : many2one "product.template"
    * type_ids : many2many "clickpublish.article.type"
    * website_ids : many2many "clickpublish.website"
}

entity product.template {
    product_ids : one2many "clickpublish.article"
}

entity clickpublish.website {
    * name : char
}

res.users ||--o{ clickpublish.article.request

hr.employee ||--o{ clickpublish.article

product.template ||--o{ clickpublish.article
product.template ||--o{ clickpublish.article.request

clickpublish.website ||--o{ clickpublish.article
clickpublish.website }o-left-o{ clickpublish.article.request

clickpublish.article.type ||--o{ clickpublish.article
clickpublish.article.type ||--o{ clickpublish.article.request

clickpublish.article.request |o--o{ clickpublish.article
@enduml
```

Permintaan artikel (`clickpublish.article.request`) berelasi dengan pengguna
(`res.users`) agar dapat digunakan untuk informasi pembuatan dan pemrosesan
permintaan. Sedangkan daftar artikel (`clickpublish.article`) berelasi dengan
biodata karyawan (`hr.employee`) agar dapat memungkinkan pencatatan penulis
artikel selain dari pengguna yang sedang login.